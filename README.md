# About

[Spring.io](http://spring.io) goes [vertx.io](http://vertx.io) example, including the [vertx OpenAPI 3 Module](https://vertx.io/docs/vertx-web-api-contract/java/#_openapi_3)

The goal of this project is to ease of the development process of well defined REST API's using a modern, scaleable and high performant engine. 

- The [SpexOpenAPI](https://bitbucket.org/moritzthielcke/spex/src/master/SpexOpenAPI/) package provides interfaces and an API builder
- The [SpexBootExample](https://bitbucket.org/moritzthielcke/spex/src/master/SpexBootExample/) uses [Spring Boot](https://spring.io/projects/spring-boot) to build a standalone REST-Service

### OpenAPI 3
The [vert OpenAPI 3 Module](https://vertx.io/docs/vertx-web-api-contract/java/#_openapi_3) provides
 
- OpenAPI 3 compliant API specification validation with automatic loading of external Json schemas
- automatic request validation
- automatic mount of security validation handlers
- automatic 501 reponses for API operations which have not yet been implemented

### Vertx
- [blazing fast](https://www.techempower.com/benchmarks/#section=data-r17&hw=ph&test=db)

- scaleable (from minimal hardware to cluster)
- event driven and non blocking
- polyglot
- unopinionated and general purpose 
- rich [web api](https://vertx.io/docs/vertx-web/java/)

### Todo
- Security Handler with [JWT Example](https://bitbucket.org/moritzthielcke/springboot_vertx/src/master/src/main/java/spex/core/cmp/auth/JWTAuthImpl.java)


### Example

Let's define a simple API operation:

``` yaml
...
  /find:
    get:
      summary: finds Pets
      operationId: findPet
      parameters:
        - name: name
          in: query
          description: Name to find
          required: true
          schema:
            type: string
      responses:
        '200':
          content:
            application/json:
              schema:
                type: string[]
          description: array of pets
```
Let write an even simpler implementation:

``` Java
public class FindPet implements APIOperation {

    @Override
    public String operationId() {
        return "findPet";
    }

    @Override
    public void handle(RoutingContext ctx) {
        List<String> result = new ArrayList<>();
        PETS.stream().filter(pet -> pet.startsWith(ctx.request().getParam("name"))).forEach(result::add);
        ctx.response().end(Json.encode(result));
    }
}
```

### Get started:
- build the project
- head over to [SpexBootExample](https://bitbucket.org/moritzthielcke/spex/src/master/SpexBootExample/).
- start the Application (run Application.java in your IDE, or the use spring-boot:run, or use the generated .jar under target/)
- take a look at the API specification for [myopenapiservice](https://bitbucket.org/moritzthielcke/spex/src/master/SpexBootExample/src/main/resources/myopenapiservice.yaml)
- dive into the code