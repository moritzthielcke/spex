package io.spex.example.service.handler.operations;

import io.spex.core.openapi.APIOperation;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.json.Json;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class ListPets implements APIOperation {
    public static final List<String> PETS = Arrays.asList("tick","trick","track");

    @Override
    public String operationId() {
        return "listPets";
    }

    @Override
    public void handle(RoutingContext ctx) {
        ctx.response().end(Json.encode(PETS));
    }
}
