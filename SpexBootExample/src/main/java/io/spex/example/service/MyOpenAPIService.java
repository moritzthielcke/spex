package io.spex.example.service;

import io.spex.core.http.error.ServerErrorHandler;
import io.spex.core.openapi.APIBuilder;
import io.spex.core.openapi.APIHandler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyOpenAPIService {
    private static final Logger log = LoggerFactory.getLogger(MyOpenAPIService.class);
    private List<APIHandler> apiHandler;
    private List<ServerErrorHandler> errorHandler;
    private ApplicationContext appContext;
    private Vertx vertx;


    public MyOpenAPIService(@Autowired List<APIHandler> apiHandler, @Autowired List<ServerErrorHandler> errorHandler,
                            @Autowired Vertx vertx, @Autowired ApplicationContext appContext) {
        this.apiHandler = apiHandler;
        this.errorHandler = errorHandler;
        this.appContext = appContext;
        this.vertx = vertx;
    }


    /***
     * start the API Service
     *
     * @param schema
     * @param port
     */
    public HttpServer startService(String schema, int port){
        return startService(schema, port, null);
    }

    /***
     * start the API Service
     * @param schema
     * @param port
     * @param options
     * @return
     */
    public HttpServer startService(String schema, int port, HttpServerOptions options){
        /* create router from api */
        Router router;
        try {
            router = new APIBuilder(schema, vertx).bind(apiHandler).addErrorHandler(errorHandler).router();
        } catch (Throwable throwable) {
            log.error("error loading api spec ", throwable);
            SpringApplication.exit(appContext, () -> -1);
            return null;
        }
        /* start http server and enjoy ! */
        HttpServer server = (options != null) ? vertx.createHttpServer(options) : vertx.createHttpServer();
        server.requestHandler(router).listen(port, serverAsyncResult -> {
            log.info("server is up -> {}", serverAsyncResult.succeeded());
            if (serverAsyncResult.failed()) {
                log.error("error starting server", serverAsyncResult.cause());
                SpringApplication.exit(appContext, () -> -1);
            }
        });
        return server;
    }
}
