package io.spex.example.service.handler.operations.errorhandling;

import io.spex.core.openapi.APIOperation;
import io.vertx.ext.web.RoutingContext;
import org.springframework.stereotype.Component;

@Component
/***
 * throws an error which is later catched by @see ThrowErrorFailureHandler
 */
public class Rollback implements APIOperation {
    @Override
    public String operationId() {
        return "rollback";
    }

    @Override
    public void handle(RoutingContext ctx) {
        throw new RuntimeException("hi from endpoint!");
    }
}
