package io.spex.example.service.handler.operations.errorhandling;

import io.spex.core.openapi.APIOperation;
import io.vertx.ext.web.RoutingContext;
import org.springframework.stereotype.Component;

@Component
/***
 * throws an uncatched error
 */
public class ThrowError implements APIOperation {
    @Override
    public String operationId() {
        return "error";
    }

    @Override
    public void handle(RoutingContext ctx) {
        throw new RuntimeException("hi from endpoint");
    }
}
