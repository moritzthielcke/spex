package io.spex.example.service.handler.operations;

import io.spex.core.openapi.APIOperation;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FindPet implements APIOperation {

    @Override
    public String operationId() {
        return "findPet";
    }

    @Override
    public void handle(RoutingContext ctx) {
        List<String> result = new ArrayList<>();
        ListPets.PETS.stream().filter(pet -> pet.startsWith(ctx.request().getParam("name"))).forEach(result::add);
        ctx.response().end(Json.encode(result));
    }
}
