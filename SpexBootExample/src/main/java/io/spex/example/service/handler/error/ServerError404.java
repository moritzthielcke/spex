package io.spex.example.service.handler.error;

import io.spex.core.http.error.NotFoundHandler;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/***
 * This class is used to log 404 errors.
 */
@Component
public class ServerError404 extends NotFoundHandler {
    private static final Logger log = LoggerFactory.getLogger(ServerError404.class);

    @Override
    public void handle(RoutingContext routingContext) {
        log.info("404 not found on {}", routingContext.request().path());
        routingContext.response().end("not found");
    }
}
