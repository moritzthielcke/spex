package io.spex.example.service.handler.global;


import io.spex.core.openapi.GlobalHandler;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GlobalRequestHandler implements GlobalHandler {
    private static final Logger log = LoggerFactory.getLogger(GlobalRequestHandler.class);
    @Override
    public void handle(RoutingContext routingContext) {
        log.info("hello from GlobalHandler {}" , routingContext.request().remoteAddress().host());
        routingContext.next();
    }
}
