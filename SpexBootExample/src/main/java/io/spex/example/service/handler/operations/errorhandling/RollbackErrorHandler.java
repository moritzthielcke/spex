package io.spex.example.service.handler.operations.errorhandling;

import io.spex.core.openapi.APIOperationFailure;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RollbackErrorHandler implements APIOperationFailure {
    private static final Logger log = LoggerFactory.getLogger(APIOperationFailure.class);

    @Override
    public String operationId() {
        return "rollback";
    }

    @Override
    public void handle(RoutingContext routingContext) {
        log.error("an error occurred:( {} {}, Rolling Back!", routingContext.statusCode(), routingContext.failed());
        //you can rollback your stuff here before doing something else
        routingContext.response().setStatusCode(routingContext.statusCode()).end();
    }
}
