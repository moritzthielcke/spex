package io.spex.example.service.handler.operations;

import io.spex.core.openapi.APIOperation;
import io.vertx.core.Vertx;
import io.vertx.ext.dropwizard.MetricsService;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShowMetric implements APIOperation {
    private static final Logger log = LoggerFactory.getLogger(ShowMetric.class);
    private MetricsService metricsService;
    private Vertx vertx;

    public ShowMetric(@Autowired Vertx vertx) {
        this.metricsService = MetricsService.create(vertx);
        this.vertx = vertx;
    }

    @Override
    public String operationId() {
        return "metric";
    }

    @Override
    public void handle(RoutingContext routingContext) {
        routingContext.response().headers().set("Content-Type", "application/json");
        routingContext.response().end(metricsService.getMetricsSnapshot(vertx).encode());
    }
}
