package io.spex.example.service.handler.error;

import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import io.spex.core.http.error.InternalServerErrorHandler;

/***
 * this class is used to log InternalServerError's
 */
@Component
public class ServerError500 extends InternalServerErrorHandler {
    private static final Logger log = LoggerFactory.getLogger(ServerError500.class);

    @Override
    public void handle(RoutingContext ctx) {
        log.error("500 occurred on {}, ooh noo's ", ctx.request().path(), ctx.failure());
    }
}
