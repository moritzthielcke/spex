package io.spex.core.http.error;

/***
 * Http Server Error 500
 */
public abstract class NotFoundHandler implements ServerErrorHandler {
    /**
     *
     * @return the status code this handler is for
     */
    public int code(){
        return 404;
    }
}
