package io.spex.core.http.error;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

/***
 * Http Server Error
 */
public interface ServerErrorHandler extends Handler<RoutingContext> {
    /**
     *
     * @return the status code this handler is for
     */
    public int code();
}
