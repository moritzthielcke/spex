package io.spex.core.openapi;

import io.swagger.v3.parser.core.models.AuthorizationValue;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.spex.core.http.error.ServerErrorHandler;

import java.util.Arrays;
import java.util.List;

/***
 *
 * Helps you to bind implementations to your OpenAPI
 *
 */
public class APIBuilder {
    private static final Logger log = LoggerFactory.getLogger(APIBuilder.class);
    private AsyncResult<OpenAPI3RouterFactory> factoryResult;
    private Router router;

    /***
     *
     * @param schema
     * @param vertx
     */
    public APIBuilder(String schema, Vertx vertx) {
        OpenAPI3RouterFactory.create(vertx, schema, ar -> {
            if (ar.succeeded()) {
                log.info("created OpenAPI factory from schema {} successfully!", schema);
                this.factoryResult =  ar;
            }
            this.ready();
        });
    }

    /**
     * api is ready
     */
    private synchronized void ready() {
        notify();
    }


    /***
     *
     * @return the OpenAPI3RouterFactory created from your api spec
     * @throws Throwable error that happened on API creation
    */
    public synchronized OpenAPI3RouterFactory factory() throws Throwable {
        if (factoryResult == null) {
            wait();
        }
        if (this.factoryResult.succeeded()) {
            return factoryResult.result();
        } else {
            throw factoryResult.cause();
        }
    }


    /**
     * returns the router for the API
     *
     * @return
     * @throws Throwable error on API creation
     */
    public Router router() throws Throwable {
        if(router==null){
            router = factory().getRouter();
        }
        return router;
    }


    /***
     * bind API Handlers
     * @param handlers
     * @return
     * @throws Throwable error on API creation
     */
    public APIBuilder bind(APIHandler... handlers) throws Throwable {
        return bind(Arrays.asList(handlers));
    }

    /**
     * Adds error handler to the router
     * @param errorHandlers
     * @return
     * @throws Throwable error on API creation
     */
    public APIBuilder addErrorHandler(ServerErrorHandler... errorHandlers)  throws Throwable{
        return addErrorHandler(Arrays.asList(errorHandlers));
    }

    /***
     * bind API handlers
     * @param handlers
     * @return
     * @throws Throwable error on API creation
     */
    public APIBuilder bind(List<APIHandler> handlers) throws Throwable {
        OpenAPI3RouterFactory factory = factory();
        log.info("binding API Handler");
        handlers.forEach(apiHandler -> {
            String implName = apiHandler.getClass().getSimpleName();
            if (apiHandler instanceof GlobalHandler) {
                GlobalHandler handler = (GlobalHandler) apiHandler;
                log.info(" adding global handler : {} ", implName);
                factory.addGlobalHandler(handler);
            } else if (apiHandler instanceof SecurityHandler) {
                SecurityHandler handler = (SecurityHandler) apiHandler;
                log.info(" adding security handler : {} -> {}", handler.schema(), implName);
                factory.addSecurityHandler(handler.schema(), handler);
            } else if (apiHandler instanceof APIOperationFailure) {
                APIOperation handler = (APIOperationFailure) apiHandler;
                log.info(" adding failure handler for api operation : {} -> {}", handler.operationId(), implName);
                factory.addFailureHandlerByOperationId(handler.operationId(), handler);
            } else if (apiHandler instanceof APIOperation) {
                APIOperation handler = (APIOperation) apiHandler;
                log.info(" adding operation : {} -> {}", handler.operationId(), implName);
                factory.addHandlerByOperationId(handler.operationId(), handler);
            } else {
                log.error("Handler Type {} is not yet implemented", implName);
            }
        });
        return this;
    }


    /**
     *  Adds error handler to the router
     *
     * @param errorHandlers
     * @return
     * @throws Throwable error on API creation
     */
    public APIBuilder addErrorHandler(List<ServerErrorHandler> errorHandlers) throws Throwable{
        Router router = router();
        errorHandlers.forEach( handler -> {
            log.info(" adding error handler {} -> {}", handler.code(), handler.getClass().getSimpleName());
            router.errorHandler(handler.code(),handler);
        });
        return this;
    }


}
