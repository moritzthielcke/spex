package io.spex.core.openapi;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

public interface APIOperationFailure extends APIOperation{
}
